var Qv = Qv || {},
Qva = Qva || {};

function fixedArea() {
	var _this = this;
	_this.htmlElement= this.Element;
	if( !$(_this.Element).parents(".QvFrame").hasClass("parentFixBlock") ) {
		_this.currId = $(_this.Element).parents(".QvFrame").addClass("parentFixBlock").attr("id");
		$(document).ready(function(){
			var minX = $(_this.Element).offset().left,
				minY = $(_this.Element).offset().top - 25,
				maxX = $(_this.Element).width() + minX,
				maxY = $(_this.Element).height() + minY;

			_this.objId = "fixedAreaDIV";
			$(_this.htmlElement).empty();
			var container = document.createElement('div');
	    	container.setAttribute('id', _this.objId);
			_this.htmlElement.appendChild(container);

			_this.filteredElements = function(){
				return $(".QvFrame").filter(function(){
					var $this = $(this);
					var Y = parseFloat($this.css("top")),
						X = parseFloat($this.css("left"));
					if( _this.currId != $this.attr("id") && !$this.hasClass("DS") && !$this.hasClass("PopupSearch") )
						return  X > minX && X < maxX && Y > minY && Y < maxY;
				});
			};
			_this.fixFilteredElements = function($filteredElements){
				$filteredElements.each(function(){
					$fixedDIV.append($(this).css({
						left: "-=" + minX + "px",
					}));
				});
			};
			_this.fixDIVs = function(){
				var $fixedDIVs = $(".parentFixBlock");
				$fixedDIVs.each(function(){
					$fixedDIV = $(this).find(".fixedDIV");
					minX = $(this).offset().left - $(document).scrollLeft(),
					minY = $(this).offset().top - 25 - $(document).scrollTop(),
					maxX = $(this).width() + minX,
					maxY = $(this).height() + minY;
					$filteredElements = _this.filteredElements();
					_this.fixFilteredElements($filteredElements);
				});
			};

			var $fixedDIV = $("<div>", {
				class: "fixedDIV",
				css: {
					position: "fixed",
					backgroundColor: "white",
					zIndex: "200000",
					top: 25,
				}
			}).appendTo( $(_this.Element).find("#" + _this.objId) );
			
			_this.fixDIVs();
			
			if( $("html").attr("clickToFix") != "true" ){
				$(document).on("click", ".QvFrame",  function(e){
					setTimeout(function(){
						_this.fixDIVs();
					}, 500);
					$("html").attr("clickToFix", "true");
				});
			};

			$(window).scroll(function(){
				$(".PopupSearch").css({
					display: "none",
				})
			});
		});
	}
};


function start_fixedArea() {
	Qv.AddExtension('fixedArea', fixedArea);
	var extensionPathOSM = Qva.Remote + (Qva.Remote.indexOf('?') >= 0 ? '&' : '?') + 'public=only&name=Extensions/fixedArea/';
    Qva.LoadCSS(extensionPathOSM + 'style.css');  
};
start_fixedArea();

